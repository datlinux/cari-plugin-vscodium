#!/usr/bin/env bash

set -euo pipefail

GH_REPO="https://github.com/VSCodium/vscodium"
TOOL_NAME="codium"
TOOL_TEST="codium"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | awk '{print $2}'
}

list_github_tags() {
  git ls-remote --tags --refs "$GH_REPO" |
    grep -o 'refs/tags/.*' | cut -d/ -f3- |
    sed 's/^v//'
}

list_all_versions() {  
	list_github_tags
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"
  rm -rf "$filename" &
  url="$GH_REPO/releases/download/${version}/VSCodium-linux-x64-${version}.tar.gz"
  echo "* Downloading $TOOL_NAME release $version..."
  curl "${curl_opts[@]}" -o "$filename" -C - "$url" || fail "Could not download $url"
  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only"
  fi

  (
    mkdir -p "$install_path/bin"
    cp -r "$CARI_DOWNLOAD_PATH"/* "$install_path"
    touch "$install_path/bin/vscodium"
    echo "#!/bin/bash" >> "$install_path/bin/vscodium"
    echo "$install_path/bin/codium --no-sandbox \"\$@\"" >> "$install_path/bin/vscodium"
    chmod a+x "$install_path/bin/vscodium"
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    mkdir -p "$HOME/.vscode-oss/extensions" &>/dev/null
    echo "Installing Jupyter, Python, R, and Julia extensions, please be patient.."
    bash "$install_path/bin/codium" --extensions-dir "$HOME/.vscode-oss/extensions" --install-extension ms-toolsai.jupyter
    bash "$install_path/bin/codium" --extensions-dir "$HOME/.vscode-oss/extensions" --install-extension julialang.language-julia
    bash "$install_path/bin/codium" --extensions-dir "$HOME/.vscode-oss/extensions" --install-extension posit.shiny
    bash "$install_path/bin/codium" --extensions-dir "$HOME/.vscode-oss/extensions" --install-extension quarto.quarto
    bash "$install_path/bin/codium" --extensions-dir "$HOME/.vscode-oss/extensions" --install-extension reditorsupport.r
    bash "$install_path/bin/codium" --extensions-dir "$HOME/.vscode-oss/extensions" --install-extension ms-python.python
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
